﻿#include <iostream>
#include <ctime>


int main()
{
	const int N = 5;
	int array[N][N];


	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			array[i][j] = i + j;
		}
	}
	
	
	std::cout << "\n";
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			std::cout << array[i][j] << "";
		}
		std::cout << std::endl;
	}

	time_t currentTime = time(NULL);
	struct tm buf;
	localtime_s(&buf, &currentTime);
	int currentDay = buf.tm_mday;
	int rowIndex = currentDay % N;
	int sum = 0;

	for (int j = 0; j < N; j++)
	{
		sum += array[rowIndex][j];
	}
	std::cout << rowIndex << ":" << sum << std::endl;
	return 0;
}
